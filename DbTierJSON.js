var express = require("express");
var fs = require('fs');
const bodyParser = require('body-parser');
var cors = require("cors");
var app =express();
var AddElementRest = require("./REST CRUD/AddElementRest.js");
var ReadElementByCodeRest = require("./REST CRUD/ReadElementByCodeRest.js");
var ReadAllElementsRest = require("./REST CRUD/ReadAllElementsRest.js");
var UpdateElementRest = require("./REST CRUD/UpdateElementRest.js");
var CancelElementRest = require("./REST CRUD/CancelElementRest.js");
var SendMailRest = require("./MAIL SERVICES/SendMailRest.js");
var AddTransactionRest = require("./PAYMENT SERVICES/AddTransactionRest.js");


app.use(cors());
app.use(bodyParser.json());


app.post("/addNewElement",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling addNewElement!");
		console.log("REQUEST",request.body)
		AddElementRest.data.addElementExecuter(request.body,response,resolve,reject);
	}).catch((e)=>{
		    console.log(e);
					}).finally(()=>{
						response.send(response.status);
						console.log("Fine servizio ADD NEW ELEMENT con esito", response.status);

					});
});

app.post("/readElementByCode",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling readElementByCode!");
		console.log("REQUEST",request.body)
		ReadElementByCodeRest.data.readElementByCodeExecuter(request.body,response,resolve,reject);
	}).catch((e)=>{
		    console.log(e);
					}).finally(()=>{
						response.send(response.body);
						console.log("Fine servizio READ ELEMENT BY CODE con esito", response.status);
					});
});


app.get("/readAllElements",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling readAllElements!");
		console.log("REQUEST",request.body)
		ReadAllElementsRest.data.readAllElementsExecuter(request.body,response,resolve,reject);
	}).catch((e)=>{
		    console.log(e);
					}).finally(()=>{
						response.send(response.body);
						console.log("Fine servizio  READ ALL ELEMENTS con esito", response.status);
					});
});



app.post("/updateElement",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling updateElement!");
		console.log("REQUEST",request.body)
		UpdateElementRest.data.updateElementExecuter(request.body,response,resolve,reject);
	}).catch((e)=>{
		    console.log(e);
					}).finally(()=>{
						response.send(response.status);
						console.log("Fine servizio  UPDATE ELEMENT con esito", response.status);
					});
});



app.post("/cancelElement",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling cancelElement!");
		console.log("REQUEST",request.body)
		CancelElementRest.data.cancelElementExecuter(request.body,response,resolve,reject);
	}).catch((e)=>{
		    console.log(e);
					}).finally(()=>{
						response.send(response.status);
						console.log("Fine servizio  CANCEL ELEMENT con esito", response.status);
					}); 
});



app.post("/sendMail",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling sendMail!");
		console.log("REQUEST",request.body);
		SendMailRest.data.sendMailExecuter(request.body,response,resolve,reject);

	}).catch((e)=>{
		    console.log(e);
					}).then(()=>{
						// response.send(response.status);
						// console.log("Fine servizio SEND MAIL con esito", response.status);
					}); 
});





app.post("/addTransaction",function(request,response){
	return new Promise((resolve,reject)=>{
		console.log("Calling addTransaction!");
		console.log("REQUEST",request.body)
		AddTransactionRest.data.addTransactionExecuter(request.body,response,resolve,reject);
	}).catch((e)=>{
		    console.log(e);
					}).finally(()=>{
						response.send(response.status);
						console.log("Fine servizio ADD TRANSACTION con esito", response.status);
					}); 
});




app.listen(5000);

    

    

    


