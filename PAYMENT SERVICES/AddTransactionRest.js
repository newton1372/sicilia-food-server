const  fs = require('fs');
var jsonElement={};
var jsonObj=null;
var request=null;
var response=null;
var resolveFun=null;
var rejectFun=null;
var urlTransactionDb="Desktop/sicilia-food-server/DATA/TransactionDB.json";

var init = function(req, resp){
    resp.status="KO";
    request=req;
    response=resp;
    fs.readFile(urlTransactionDb,"utf8", function(err,data){
        addElement(err,data);
    });
 
}





var manageNewElementAnsw = function(result){
    jsonElement=result;    
    jsonObj.push(jsonElement);
    
    fs.writeFile(urlTransactionDb, JSON.stringify(jsonObj), "utf8",function(){
        console.log("Transazione aggiunta!");
        response.status="OK";
        resolveFun();
    });
}

var addElement = function(err,data){
    if(err) rejectFun("ERRORE NELLA LETTURA DEL FILE");	
    else{
        jsonObj = JSON.parse(data);
        manageNewElementAnsw(request);
    }
}
              


exports.data = {
    addTransactionExecuter: function(req,resp,resolve,reject){
        resolveFun = resolve;
        rejectFun = reject;
        init(req, resp);
    }
};
